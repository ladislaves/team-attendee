import AttendeeItemInterface from "./attendeeItemInterface";

export interface TeamActionInterface {
    type: string;
    attendee: AttendeeItemInterface;
    attendeeList: Array<AttendeeItemInterface>;
    title: string;
}

export default interface AttendeeItemInterface {
    name: string;
    status: boolean;
}

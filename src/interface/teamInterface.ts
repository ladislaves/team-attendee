import AttendeeItemInterface from "./attendeeItemInterface";

export default interface TeamInterface {
    title: string;
    attendeeList?: Array<AttendeeItemInterface>;
}

import AttendeeItemInterface from "./attendeeItemInterface";

export default interface AttendeeListInterface {
    items?: Array<AttendeeItemInterface>
}

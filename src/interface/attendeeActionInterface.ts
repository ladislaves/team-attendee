export default interface AttendeeActionInterface {
    setStatus: (name: string, status: boolean) => void;
}

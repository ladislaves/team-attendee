import Redux from 'redux';
import attendeeListFixture from '../fixture/players';
import { INIT_ATTENDEES } from '../reducer/team';

const asyncMockService = (timeout: number = 2000) =>
    new Promise((resolve) => setTimeout(
        () => resolve(attendeeListFixture), timeout)
    );

export default () => async (dispatch: Redux.Dispatch<any>) => {
    const response = await asyncMockService();

    dispatch({
        type: INIT_ATTENDEES,
        attendeeList: response,
    });
};

import Redux from 'redux';
import { ADD_ATTENDEE } from '../reducer/team';

export default (name: string, status: boolean) => (dispatch: Redux.Dispatch<any>) => {
    dispatch({
        type: ADD_ATTENDEE,
        attendee: { name, status },
    });
};

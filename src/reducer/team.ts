import { TeamActionInterface } from "../interface/teamActionInterface";
import TeamInterface from "../interface/teamInterface";

export const INIT_ATTENDEES = 'init_attendees';
export const ADD_ATTENDEE = 'add_attendee';
export const SET_TITLE = 'set_title';

export const defaultState: TeamInterface  = {
    title: '',
};

const team = (state: TeamInterface = defaultState, action: TeamActionInterface) => {
    switch (action.type) {
        case INIT_ATTENDEES:
            return {
                ...state,
                attendeeList: action.attendeeList,
            };
        case ADD_ATTENDEE:
            const { attendeeList = [] } = state;

            return {
                ...state,
                attendeeList: [...attendeeList, action.attendee],
            };
        case SET_TITLE:
            return {
                ...state,
                title: action.title,
            };
        default:
            return state;
    }
};

export default team;

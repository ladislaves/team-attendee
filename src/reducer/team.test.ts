import React from 'react';
import teamReducer, {ADD_ATTENDEE, INIT_ATTENDEES, SET_TITLE} from './team';
import attendeeListFixture from '../fixture/players';

const newStateMock = {
        title: 'test title',
        attendee: { name: 'Omar', status: true },
        attendeeList: attendeeListFixture,
};

describe('Team reducer test', () => {
    it('Should init the state', () => {
        const state = teamReducer(undefined, {});
        expect(state).toMatchObject({ title: '' });
    });

    it('Should add title', () => {
        const state = teamReducer(
            undefined,
            {
                type: SET_TITLE,
                ...newStateMock
            },
        );
        expect(state).toMatchObject({ title: 'test title' });
    });

    it('Should init the collection', () => {
        const state = teamReducer(
            undefined,
            {
                type: INIT_ATTENDEES,
                ...newStateMock
            },
        );
        expect(state).toMatchObject({ title: '', attendeeList: attendeeListFixture });
    });

    it('Should add attendee into the collection', () => {
        const attendee = { name: 'Omar', status: true };
        const state = teamReducer(
            undefined,
            {
                type: ADD_ATTENDEE,
                ...newStateMock
            },
        );
        expect(state).toMatchObject({ title: '', attendeeList: [attendee] });
    });
});

import React, { Component } from 'react';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import './App.css';
import TeamReducer, {defaultState} from './reducer/team';
import Team from './container/teamContainer';
import TeamInterface from './interface/teamInterface';

// Assume we have got initial state from login or so.
const initialState: TeamInterface = {
    title: 'Brand New Team',
};

const store = createStore(
    TeamReducer,
    initialState,
    compose(
        applyMiddleware(thunk),
        (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
    ),
);

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <h1>Team Attendee</h1>
                    <Team />
                </div>
            </Provider>
        );
    }
}

export default App;

import React from 'react';
import renderer from 'react-test-renderer';
import TeamName from './teamName';

describe('TeamName render test', () => {
    it('should render team name', () => {
        const component = renderer.create(<TeamName title="My team title" />);
        expect(component.toJSON()).toMatchSnapshot();
    });
});

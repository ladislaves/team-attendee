import React from 'react';
import Status from "./status";
import AttendeeItemInterface from "../interface/attendeeItemInterface";

const AttendeeItem: React.FC<AttendeeItemInterface> = ({ name, status }) => (
    <li>
        <strong className="name">{name}</strong>
        <Status status={status} />
    </li>
);

export default AttendeeItem;

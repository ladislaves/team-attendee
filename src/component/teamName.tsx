import React from 'react';
import TeamInterface from "../interface/teamInterface";

const TeamName: React.FC<TeamInterface> = ({ title }) => <h2>Your team: {title}</h2>;

export default TeamName;

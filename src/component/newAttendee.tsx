import React from 'react';
import AttendeeActionInterface from "../interface/attendeeActionInterface";
import { Button } from 'react-bootstrap';

interface NewAttendeeStateInterface {
    name: string;
}

class NewAttendee extends React.Component<AttendeeActionInterface, NewAttendeeStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            name: '',
        };
    }

    setName(event: any) {
        this.setState({
            name: event.target.value,
        });
    }

    addAttendee(name: string, status: boolean) {
        this.props.setStatus(name,status);
        this.setState({name: ''});
    }

    render() {
        const { setStatus } = this.props;
        const { name } = this.state;

        return (
          <>
              <input placeholder="Your name" value={name} onChange={event => this.setName(event)} />
              <Button variant="success" onClick={() => this.addAttendee(name,true)}>Yes</Button>
              <Button variant="secondary" onClick={() => this.addAttendee(name,false)}>Not this week</Button>
          </>
        );
    }
}

export default NewAttendee;

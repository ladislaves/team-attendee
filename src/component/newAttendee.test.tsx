import React from 'react';
import renderer from 'react-test-renderer';
import NewAttendee from './newAttendee';

describe('NewAttendee render test', () => {
    it('should render new attendee form', () => {
        const component = renderer.create(<NewAttendee setStatus={jest.fn} />);
        expect(component.toJSON()).toMatchSnapshot();
    });
});

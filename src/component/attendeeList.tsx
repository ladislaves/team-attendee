import React from 'react';
import AttendeeListInterface from '../interface/attendeeListInterface';
import AttendeeItemInterface from '../interface/attendeeItemInterface';
import AttendeeItem from './attendeeItem';
import AttendeeSummary from './attendeeSummary';

const AttendeeList: React.FC<AttendeeListInterface> = ({
    items,
}) => (
    <ul>
        {items && items.map((item: AttendeeItemInterface) => <AttendeeItem key={item.name} name={item.name} status={item.status} />)}
        {items && <AttendeeSummary items={items} />}
    </ul>
);

export default AttendeeList;

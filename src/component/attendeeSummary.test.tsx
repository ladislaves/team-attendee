import React from 'react';
import renderer from 'react-test-renderer';
import AttendeeSummary from './attendeeSummary';
import listFixture from '../fixture/players';

describe('AttendeeSummary render test', () => {
    it('should render attendee summary', () => {
        const component = renderer.create(<AttendeeSummary items={listFixture} />);
        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should render empty attendee summary', () => {
        const component = renderer.create(<AttendeeSummary items={[]} />);
        expect(component.toJSON()).toMatchSnapshot();
    });
});

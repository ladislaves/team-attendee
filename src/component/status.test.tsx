import React from 'react';
import renderer from 'react-test-renderer';
import Status from './status';

describe('Status render test', () => {
    it('should render positive status', () => {
        const component = renderer.create(<Status status={true} />);
        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should render negative status', () => {
        const component = renderer.create(<Status status={false} />);
        expect(component.toJSON()).toMatchSnapshot();
    });
});

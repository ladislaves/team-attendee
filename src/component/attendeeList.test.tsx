import React from 'react';
import renderer from 'react-test-renderer';
import AttendeeList from './attendeeList';
import listFixture from '../fixture/players';

describe('AttendeeList render test', () => {
    it('should render list', () => {
        const component = renderer.create(<AttendeeList items={listFixture} />);
        expect(component.toJSON()).toMatchSnapshot();
    });
});

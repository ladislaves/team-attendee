import React from 'react';
import renderer from 'react-test-renderer';
import AttendeeItem from './attendeeItem';

describe('AttendeeItem render test', () => {
    it('should render item', () => {
        const component = renderer.create(<AttendeeItem name={'Liselota'} status={true} />);
        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should render item', () => {
        const component = renderer.create(<AttendeeItem name={'Ole'} status={false} />);
        expect(component.toJSON()).toMatchSnapshot();
    });
});

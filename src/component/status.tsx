import React from 'react';
    import { Badge } from 'react-bootstrap';
import StatusInterface from '../interface/statusInteface';

const Status: React.FC<StatusInterface> = ({ status }) => (
  <Badge variant={status ? 'success' : 'secondary'}>
      {status ? 'Yes' : 'No'}
  </Badge>
);

export default Status;

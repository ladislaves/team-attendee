import React from 'react';
import Badge from 'react-bootstrap/Badge';
import AttendeeListInterface from '../interface/attendeeListInterface';

const AttendeeSummary: React.FC<AttendeeListInterface> = ({
   items,
}) => {
    const count = items ? items.filter(item => item.status).length : 0;

    return (
        <li>
        <span className="total">
            Number of attendees for this week
        </span>
            <Badge variant="primary">{count}</Badge>
        </li>
    );
}

export default AttendeeSummary;

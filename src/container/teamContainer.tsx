import React from 'react';
import { connect } from 'react-redux';
import TeamInterface from '../interface/teamInterface';
import setStatus from '../action/setStatus';
import TeamName from "../component/teamName";
import NewAttendee from "../component/newAttendee";
import AttendeeList from "../component/attendeeList";
import TeamContainerInterface from '../interface/teamContainerInterface';
import getAttendee from '../action/getAttendee';

class Team extends React.Component<TeamInterface & TeamContainerInterface> {
    constructor(props: any) {
        super(props);
    }

    componentDidMount() {
        this.props.getAttendee();
    }

    render() {
        const { title, setStatus, attendeeList } = this.props;

        return (
            <>
                <TeamName title={title}/>
                <NewAttendee setStatus={setStatus}/>
                {!attendeeList && <p>loading ...</p>}
                {attendeeList && <AttendeeList items={attendeeList}/>}
            </>
        );
    }
}

const mapStateToProps = (state: TeamInterface) => ({
    title: state.title,
    attendeeList: state.attendeeList,
});

const mapDispatchToProps = {
    setStatus,
    getAttendee,
};

export default connect(mapStateToProps, mapDispatchToProps)(Team);

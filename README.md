# Team Attende - are we going to play this week?
Demo React application. Registration of weekly workout attendees for collectiove sports (football, floorball, etc.).

# Basic usage

**Run application**

1.  checkout repository
2.  run the application:


`yarn`

and then

`yarn start`

If the browser doesn't start automatically then open http://localhost:3000/ to see the application.


**Test application**

` yarn test`

or with code coverage

` yarn test --coverage`